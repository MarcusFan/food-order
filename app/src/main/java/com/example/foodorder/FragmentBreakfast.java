package com.example.foodorder;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class FragmentBreakfast extends Fragment {

    public FragmentBreakfast() {
        // Required empty public constructor
    }

    public static FragmentBreakfast newInstance() {
        FragmentBreakfast fragment = new FragmentBreakfast();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_breakfast, container, false);
        setupView();
        return rootView;
    }
    //Private prop
    private View rootView;
    //Private
    private void setupView(){
        RestaurantBreakfast restaurant = new RestaurantBreakfast();


        ImageView imageView = rootView.findViewById(R.id.image_view);
        TextView nameTextView = rootView.findViewById(R.id.name_text_view);
        TextView shortDescriptionTextView = rootView.findViewById(R.id.short_description_text_view);
        TextView streetAddressTextiew = rootView.findViewById(R.id.street_address_text_view);
        TextView cityTextView = rootView.findViewById(R.id.city_text_view);
        TextView phoneNumberTextView = rootView.findViewById(R.id.phone_number_text_view);
        TextView distanceTextView = rootView.findViewById(R.id.distance_text_view);

        imageView.setImageResource(restaurant.imageResource);
        nameTextView.setText(restaurant.restaurantName);
        shortDescriptionTextView.setText(restaurant.shortDescription);
        streetAddressTextiew.setText(restaurant.streetAddress);
        cityTextView.setText(restaurant.city);
        phoneNumberTextView.setText(restaurant.phoneNumber);
        distanceTextView.setText(restaurant.distance);
    }
}