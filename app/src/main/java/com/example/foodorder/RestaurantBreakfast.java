package com.example.foodorder;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

public class RestaurantBreakfast {

    // Singleton
    public RestaurantBreakfast(){setupProperties();}


    // Public properties
    public String restaurantName;
    public String shortDescription;
    public String streetAddress;
    public String city;
    public String phoneNumber;
    public String distance;
    public int imageResource;

    // Private properties
    private Lorem lorem = LoremIpsum.getInstance();

    // Private methods
    private void setupProperties() {
        restaurantName = lorem.getTitle(1, 2);
        shortDescription = lorem.getWords(3, 7);
        streetAddress = randomStreetAddress();
        city = lorem.getCity();
        phoneNumber = lorem.getPhone();
        distance = randomDistance();
        imageResource = R.drawable.restaurantbreakfast;
    }

    private String randomStreetAddress() {
        int min = 100;
        int max = 9999;
        int randomNumber = (int)(Math.random() * (max - min + 1) + min);
        String randomStreetAddress = randomNumber + " " + lorem.getCity() + " " + "Street";
        return randomStreetAddress;
    }
    private String randomDistance(){
        int min = 2;
        int max = 20;
        int randomDistanceNumber = (int)(Math.random() * (max - min + 1) + min);
        String randomDistance = randomDistanceNumber + "km";
        return randomDistance;
    }
}
