package com.example.foodorder;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class Loadupp extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadupp);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


    }
    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("Resuming ActivityMain");
        invalidateOptionsMenu();  // Refresh action bar button (cart button)

        //Identification of buttons
        ImageButton = findViewById(R.id.imageButton);
        ComfortFood = findViewById(R.id.ComfortFood);
        Asian = findViewById(R.id.Asian);
        VegDiner = findViewById(R.id.VegDiner);
        Breakfast = findViewById(R.id.Breakfast);
        CartButton = findViewById(R.id.cartbutton);

        Breakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m= new Intent(Loadupp.this, BreakfastFood.class);


                startActivity(m);


                finish();
            }
        });

        VegDiner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m= new Intent(Loadupp.this, VegeFood.class);


                startActivity(m);


                finish();
            }
        });

        Asian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m= new Intent(Loadupp.this, AsianFood.class);


                startActivity(m);


                finish();

            }
        });


        ComfortFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent m= new Intent(Loadupp.this, ComfortFood.class);


                startActivity(m);


                finish();

            }



        });

        ImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent m= new Intent(Loadupp.this, Options.class);


                startActivity(m);


                finish();

            }
        });

        CartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent m= new Intent(Loadupp.this, CartActivity.class);


                startActivity(m);



            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate our action bar layout (action_bar_menu.xml)
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbarmenu, menu);

            // Check to see if there are items in the cart
            Boolean areItemsInCart = Cart.getInstance().numberOfItems() > 0;

            // Set the cart icon to be enabled/disabled depending on whether there are items in the cart
            menu.findItem(R.id.action_cart).setEnabled(areItemsInCart);

            // Return our inflated menu
            return super.onCreateOptionsMenu(menu);
        }

        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_cart:
                    System.out.println("Selected cart");
                    //TODO: Navigate to cart screen

                    break;
                default:
                    break;
            }
            return true;








    }

    //Properties
    ImageButton ImageButton;
    Button ComfortFood;
    Button Asian;
    Button VegDiner;
    Button Breakfast;
    ImageButton CartButton;
}
