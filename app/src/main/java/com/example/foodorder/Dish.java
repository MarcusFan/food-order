package com.example.foodorder;

import androidx.annotation.NonNull;

import com.thedeanda.lorem.Lorem;
import com.thedeanda.lorem.LoremIpsum;

public class Dish {
    public String name;
    public String description;
    public Integer priceinCents;
    public String imageResourcename;


    public Dish(String imageResourcename){
        this.imageResourcename = imageResourcename;


        populateProperties();
    }

    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", priceinCents=" + priceinCents +
                ", imageResourcename='" + imageResourcename + '\'' +
                '}';
    }

    public Boolean equals(Dish dish) {
        return (name.equals(dish.name) &&
                description.equals(dish.description) &&
                imageResourcename.equals(dish.imageResourcename) &&
                priceinCents == dish.priceinCents);

        }

    private static Lorem lorem = LoremIpsum.getInstance();
    private int minPriceinCents = 799;
    private int maxPriceinCents = 1999;


    private void populateProperties() {
        String name = lorem.getTitle(1, 4);
        String description = lorem.getParagraphs(2, 4);


        this.name = name;
        this.description = description;
        this.priceinCents = (int)(Math.random() * (maxPriceinCents - minPriceinCents +1) + minPriceinCents);
    }
}
