package com.example.foodorder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ComfortFood extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comfort_food);
        LinearLayout linearLayoutContainer = findViewById(R.id.CuisineFragmentContainer);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HashMap<String, ArrayList<Dish>> dishesbycuisine = Menu.getInstance().dishesbycuisine();
        for (Map.Entry<String, ArrayList<Dish>> entry : dishesbycuisine.entrySet()){
            Cuisine cuisine = Cuisine.valueOf(entry.getKey());


            Fragment fragment = CuisineListFragment.newInstance(cuisine);

            fragmentTransaction.add(linearLayoutContainer.getId(),fragment, null);

        }

        fragmentTransaction.commit();

    }
    @Override
    protected void onResume() {
        super.onResume();

        goBack = findViewById(R.id.goback);
        CartButton = findViewById(R.id.cartbutton);

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m= new Intent(ComfortFood.this, Loadupp.class);


                startActivity(m);


                finish();
            }
        });
        CartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent m= new Intent(ComfortFood.this, CartActivity.class);


                startActivity(m);



            }
        });



    }

    ImageButton goBack;
    ImageButton CartButton;

}