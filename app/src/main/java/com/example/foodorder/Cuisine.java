package com.example.foodorder;

import androidx.annotation.NonNull;

public enum Cuisine {

    Urban("Urban"),
    Asian("Asian"),
    Vegetarian("Vegetarian"),
    CAFE("Cafe"),
    Breakfast("Breakfast");

    Cuisine(String cuisineName){
        this.cuisineName = cuisineName;

    }

    @Override
    public String toString() {
        return cuisineName;
    }

    private String cuisineName;

    }

